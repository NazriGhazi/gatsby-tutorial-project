import React from "react"
import { Link } from "gatsby"
import Title from "../Globals/Title"
export default function info() {
  return (
    <section className="py-5">
      <div className="container">
        <Title title="Our Story" />
        <div className="row">
          <div className="col-10 col-sm-8 mx-auto text-center">
            <p className="lead text-muted mb-5">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. In
              dignissimos sequi deserunt perspiciatis iure maiores ex libero
              error corrupti et, est vitae consequuntur nemo fuga aut odit quae
              quam aperiam dolore vel voluptates ut aliquid quidem quos? Tempora
              pariatur fuga recusandae ipsum? Quo nostrum temporibus quidem
              aliquam nihil nesciunt maiores!
            </p>
            <Link to="/about/">
              <button className="btn text-Uppercase btn-yellow">
                About page
              </button>
            </Link>
          </div>
        </div>
      </div>
    </section>
  )
}
